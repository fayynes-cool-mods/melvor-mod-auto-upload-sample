# melvor-mod-auto-upload-sample



## What is this?

Tired of manually uploading your file? This small utility just auto upload yours zip to Mod.io to save some headache. This is designed to be used with gitlab CI but feel free to take any parts of the file to use in anyway you with. Github, local, w/e you want. The rest of this read me will describe how to use it the script "as is", but will still be useful if you use it in other ways and don't know where to get the information needed.

## Requirements

This CI config assumes you are using the [boilerplate](https://github.com/CherryMace/melvor-idle-mod-boilerplate). As well as using Gitlab as your CI system.

## Needed Info

For this script to work you need to provide it 3 things

1. Your Mod ID - You can find this on your mod's page in mod.io as the Resource ID. This is in the same box as your raitings and download metrics.
2. Your mod.io OAUTH. You can create this on your mod.io account page, under Access. I use Read/Write but just Write *should* work.
3. Your mod name. This can really be anything you want, but to keep things clean I sugges your mod name such as "my-cool-mod"

Add all these as Variables for Gitlab under Settings -> CI/CD as MOD_ID, MOD_IO_OAUTH, MOD_NAME.

## The Workings

This file will on push to your repo build your zip file with the provided script, and then upload it to your Mod as a NON LIVE file, unless it is the very first every file, in which case it will be forced to be live.
